# Sample Project: Stripe SDK with Google Pay integration

This is a sample project to test Google Pay integration with Stripe payment gateway for the Givio Android App. The project contains one Activity with a button to start Google Pay payment for $1. We are using the test API key from Stripe, so no actual money is deducated.


We are developing an app (`applicationId`: `givio.giv_io`) that allows users to make payments throug Google Pay.

We have already applied for Google Pay integration, got approval from Google Pay team, and enabled the  app on Google Pay Developer Profile (Google Pay Merchant ID: **16858787986866825875**).


# The Problem

When starting Google Pay, it shows an error that **"This app is not enabled for Google Pay."**. 

We had several communications with the Google Pay team and verified all the checklists:

 - [x] Signed the APK with a release key (already on Google Play internal track)
 - [x] Checked the SHA1 of the release key (`59:51:3A:AC:07:AB:5F:F4:F4:61:7A:FF:8D:0A:AE:E2:E3:8A:3A:A1`)
 - [x] Enabled the application (Givio) on Google Pay Developer Profile.

The issue still exists.

One strange thing that I noticed is that, [whenever I change](https://gitlab.com/qb-givio/givio-stripe-googlepay-sample/blob/master/app/build.gradle#L13) the `applicationId` from `givio.giv_io` to some other random name, the Google Pay will work (at least in `WalletConstants.ENVIRONMENT_TEST` mode). Google Pay will not work even in test mode when the `applicationId` is `givio.giv_io`.