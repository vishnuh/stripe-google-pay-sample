package givio.giv_io

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.widget.Button
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.google.android.gms.wallet.*
import com.stripe.android.GooglePayConfig
import com.stripe.android.PaymentConfiguration
import org.json.JSONArray
import org.json.JSONObject

/*
    Strip Payment using Google Pay

    Example based on the documentation at https://stripe.com/docs/google-pay
 */
class MainActivity : AppCompatActivity() {

    private var paymentsClient: PaymentsClient? = null

    companion object {
        private const val LOAD_PAYMENT_DATA_REQUEST_CODE = 53
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val btnGooglePay = findViewById<Button>(R.id.btnGooglePay)


        btnGooglePay.setOnClickListener {
            
            PaymentConfiguration.init(this, "pk_test_w3hCIj4qy1sGMzZslj5uBvvK00sddXySyI")

            paymentsClient = Wallet.getPaymentsClient(
                this,
                Wallet.WalletOptions.Builder()
                    .setEnvironment(WalletConstants.ENVIRONMENT_TEST)
                    .build()
            )
            
            payWithGoogle()

        }

    }

    private fun payWithGoogle() {

        val client = paymentsClient ?: return
        val paymentDataRequest = createPaymentDataRequest() ?: return

        AutoResolveHelper.resolveTask(
            client.loadPaymentData(paymentDataRequest),
            this,
            LOAD_PAYMENT_DATA_REQUEST_CODE
        )
    }

    private fun createPaymentDataRequest(): PaymentDataRequest? {

        val cardPaymentMethod = JSONObject()
            .put("type", "CARD")
            .put(
                "parameters",
                JSONObject()
                    .put("allowedAuthMethods", JSONArray()
                        .put("PAN_ONLY")
                        .put("CRYPTOGRAM_3DS"))
                    .put("allowedCardNetworks",
                        JSONArray()
                            .put("AMEX")
                            .put("DISCOVER")
                            .put("JCB")
                            .put("MASTERCARD")
                            .put("VISA"))

                    // require billing address
                    .put("billingAddressRequired", false)
            )
            .put("tokenizationSpecification", GooglePayConfig(this).tokenizationSpecification)

        // create PaymentDataRequest
        val paymentDataRequest = JSONObject()
            .put("apiVersion", 2)
            .put("apiVersionMinor", 0)
            .put("allowedPaymentMethods", JSONArray().put(cardPaymentMethod))
            .put("transactionInfo", JSONObject()
                .put("totalPrice", "1.00")
                .put("totalPriceStatus", "FINAL")
                .put("currencyCode", "USD")
            )
            .put("merchantInfo", JSONObject().put("merchantName", "Example Merchant"))

            // require email address
            .put("emailRequired", false)
            .toString()

        return PaymentDataRequest.fromJson(paymentDataRequest)
    }

    public override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {

        super.onActivityResult(requestCode, resultCode, data)

        when (requestCode) {

            LOAD_PAYMENT_DATA_REQUEST_CODE -> {
                when (resultCode) {
                    Activity.RESULT_OK -> {
                        if (data != null) {
                            onGooglePayResult(data)
                        }
                    }
                    Activity.RESULT_CANCELED -> {
                        // Cancelled
                    }
                    AutoResolveHelper.RESULT_ERROR -> {
                        // Log the status for debugging
                        // Generally there is no need to show an error to
                        // the user as the Google Payment API will do that

                        //val status = AutoResolveHelper.getStatusFromIntent(data)
                    }
                    else -> {
                        // Do nothing.
                    }
                }
            }
            else -> {
                // Handle any other startActivityForResult calls you may have made.
            }
        }
    }

    private fun onGooglePayResult(data: Intent) {
        val paymentData = PaymentData.getFromIntent(data) ?: return

        // Just showing the data on a Toast.
        // We use this token to call the "charge" API via Stripe.
        Toast.makeText(this, paymentData.toJson(), Toast.LENGTH_LONG).show()
    }
}
